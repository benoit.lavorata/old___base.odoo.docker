# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('VERSION') as fd:
    version = fd.read().strip()

setup(
    name="base.odoo.docker",
    version=version,
    description="project description",
    license='GNU Affero General Public License v3 or later (AGPLv3+)',
    author="Benoit Lavorata",
    url="https://benoitlavorat.com/",
    packages=[],
    include_package_data=True,
    classifiers=[
        'GNU Affero General Public License v3 or later (AGPLv3+)',
        'Programming Language :: Python'
    ],
)